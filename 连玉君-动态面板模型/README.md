## 连享会直播：动态面板数据模型


### 课程概览

- **听课方式：** 网络直播。报名后点击邀请码进入在线课堂收看，无需安装任何软件。支持手机、iPad、电脑等。
- **直播嘉宾**：连玉君 (中山大学)
- **时长/费用**：2 小时 20 分钟，88 元
- **课件：** 报名后点击短书平台「**课程表**」查看。包括：讲义 PPT，实操 Stata 数据和程序文件。
- **课程咨询：** 李老师-18636102467（微信同号），联系客服，加入课程微信群，课后答疑和讨论。
- **[课程主页](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e)：** [「点我报名」](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e)

### 课程提要

- **简介**：为何/何时使用动态面板模型？
- **模型设定和估计方法**：FD-GMM, SYS-GMM, 纠偏 OLS
- **序列相关** 检验
- **过度识别** 检验 
- **Stata 实操**
  - OLS, FE, IV, 2SLS, GMM 对比
  - Monte Carlo 模拟分析
  - Arellano and Bond (1991) 结果重现及解读
- 实证分析中的主要 **陷阱**
  - 至少需要几年的数据？
  - 要做哪些检验？一直通不过怎么办？
  - 工具变量太多怎么办？
  - Sargan 检验的 p 值为 1 可以吗？
  - 选哪个？`xtabond`, `xtabond2`, `xtdpd`, `xtdpdsys`, `xtbcfe`
- Top 期刊 **论文解读** (2 篇)

### 课程特色

- **深入浅出**：掌握最主流的实证分析思路和方法
- **浸入式教学**：全程电子板书+Stata 操作演示，课后分享电子板书
- **数据和程序**：分享全套 Stata 课件 (PPT、数据、程序和 dofiles)，以便重现课程中演示的所有结果
 
### 关联课程
- 先导课程：[直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) [免费公开课，1小时40分]， [-课程主页-](https://gitee.com/arlionn/PanelData)，面板数据模型概览+静态面板模型。
- 先导课程：[我的特斯拉-实证研究设计](https://lianxh.duanshu.com/#/brief/course/5ae82756cc1b478c872a63cbca4f0a5e)，时长：2小时20分，[-课程主页-](https://gitee.com/arlionn/Live)。内容：如何进行规范的实证研究、模型设定、稳健性检验、内生性问题等，附带五篇论文的重现资料。    
- 后续课程：[我的甲壳虫-经典论文重现](https://lianxh.duanshu.com/#/brief/course/c3f79a0395a84d2f868d3502c348eafc)，时长：6 小时；[-课程主页-](https://gitee.com/arlionn/Paper101)，[-幻灯片+部分课件-](https://gitee.com/arlionn/paper101/wikis/Home)。内容：拆解一篇经典论文，学习研究设计、实证方法和选题切入点。

&emsp;

--- 


## 报名


> #### [长按/扫码报名](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e)：     
> https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/二维码-报名链接-直播-动态面板150.png)

### 扫码入群：更多分享和讨论
> 请扫描海报底部二维码，加入课程微信群。

&emsp;

---
## 更多精彩课程

> 连享会 Live - 直播课程主页      
> <https://gitee.com/arlionn/Live>

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/二维码-连享会直播主页-Live150.png)



&emsp;

---
>#### 关于我们
- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。
- **欢迎赐稿：** 欢迎赐稿至StataChina@163.com。录用稿件达 **三篇** 以上，即可 **免费** 获得一期 Stata 现场培训资格。
- **往期精彩推文：**
 [Stata绘图](https://mp.weixin.qq.com/s/xao8knOk0ulGfNc7vasfew) | [时间序列+面板数据](https://mp.weixin.qq.com/s/8yP1Dijylgreg59QIkqnMg) | [Stata资源](https://mp.weixin.qq.com/s/Kdeoi5uJyNtwwwptdQDQDQ) | [数据处理+程序](https://mp.weixin.qq.com/s/_3DQacFyy7juRjgFedp9WQ) |  [回归分析-交乘项-内生性](https://mp.weixin.qq.com/s/61qJNWnL4KRp0fbLxuDGww)
---
![欢迎加入Stata连享会(公众号: StataChina)](https://images.gitee.com/uploads/images/2020/0207/180042_92f438ae_1522177.png)

